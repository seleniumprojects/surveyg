package app;

 

import java.util.UUID;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

public class EditSurveyQuestion {
public static Logger LOGGER=Logger.getLogger(EditSurveyQuestion.class);
	public static void main(String[] args)  {
		try {
		
		System.out.println("Inside the main method");
		System.setProperty("webdriver.firefox.marionette","C:\\geckodriver.exe");
		//System.setProperty("webdriver.chrome.driver", "E:\\chromedriver.exe");
		//WebDriver driver = new ChromeDriver();
		WebDriver driver = new FirefoxDriver();
		driver.get("http://stg.surveygamez.com/");
		System.out.println("Successfully opened the website http://stg.surveygamez.com/");
		Thread.sleep(5000);
		//driver.manage().window().maximize();
		loginAccess(driver);
		Thread.sleep(8000);
		System.out.println("After sign in waiting");
		System.out.println("I found the element");
		searchSurvey(driver);
		//openCreateSurveyPage(driver);
	    Thread.sleep(8000);
	    //createSurvey(driver);
	    //driver.findElement(By.className("navbar-brand")).click();
	    //openProfileAndLogoutPanel(driver);
	    //logoutApp(driver);
	    Thread.sleep(8000);
	    addWelcomeQuestion(driver);
	    Thread.sleep(8000);
	    addThankYouQuestion(driver);
	    LOGGER.info("Welcome text saved ");
	   
		}catch(InterruptedException exception) {
			exception.printStackTrace();
			
		}catch(Exception exception) {
			exception.printStackTrace();
		}
			 
		
	}
	static void  loginAccess(WebDriver driver )
	{
		System.out.println("Indside the login method");
		driver.findElement(By.id("btnSignin")).click();
		driver.findElement(By.id("email")).sendKeys("vipin.gupta@sevyamultimedia.com");
		driver.findElement(By.id("password")).sendKeys("Survey@123");
		driver.findElement(By.id("signin-text")).click();
		System.out.println("Successfully sign in");
	}
	static void openCreateSurveyPage(WebDriver driver)
	{
		driver.findElement(By.id("createNewSurvey")).click();
	    System.out.println("Create survey page successfully loaded");
	}
	static void logoutApp(WebDriver driver)
	{
		driver.findElement(By.xpath("/html/body/nav/div/div[2]/div[3]/ul/li/ul/li[2]/a"));
		System.out.println("I found the Signout button");
		driver.findElement(By.xpath("/html/body/nav/div/div[2]/div[3]/ul/li/ul/li[2]/a")).click();
		System.out.println("I clicked on Signout button");
	}
	static void openProfileAndLogoutPanel(WebDriver driver)
	{
		driver.findElement(By.xpath("/html/body/nav/div/div[2]/div[3]/ul/li/a"));
		System.out.println("I found the Profile/Logout");
		driver.findElement(By.xpath("/html/body/nav/div/div[2]/div[3]/ul/li/a")).click();
	}
	static void createSurvey(WebDriver driver)
	{
		UUID uuid=UUID.randomUUID();
		driver.findElement(By.id("surveyTitle")).sendKeys("AutomationSurvey"+uuid.toString());
		System.out.println("Successfully entered title name");
		driver.findElement(By.id("firstNext")).click();
		System.out.println("New Survey created successfully");
	}
	
	static void dragAndDrop(WebDriver driver,WebElement from,WebElement to) {
		 System.out.println("drag and drop start");
		Actions builder = new Actions(driver);
		Action dragAndDrop = builder.clickAndHold(from).moveToElement(to) .release(to).build();
		dragAndDrop.perform();
	}
	
	static void addWelcomeQuestion(WebDriver driver) throws InterruptedException  {
		System.out.println("welcome question creation start");
		 	 
		 
	//	WebElement from=driver.findElement(By.cssSelector("/html/body/div[7]/div[1]/div/ul/li[1]/a/div/span"));
		driver.findElement(By.xpath("/html/body/div[7]/div[2]/div/div[1]")).click();
		//dragAndDrop(driver, from, to);
		System.out.println("welcome question created");
		addQuestionText(driver,"Welcome");
		
	}
	static void addThankYouQuestion(WebDriver driver) throws InterruptedException  {
		System.out.println("Thank You question creation start");
		
		WebElement from=driver.findElement(By.cssSelector("[data-questiontypeid='13']"));
		//list-group connected ui-sortable questionlist thankyou-question question-container
		/*WebElement to=driver.findElement(By.xpath("//div[contains(@class, 'list-group') and contains(@class, 'connected') "
				+ " and contains(@class, 'ui-sortable')"
				+ " and contains(@class, 'questionlist')"
				+ " and contains(@class, 'thankyou-question')"
				+ " and contains(@class, 'question-container')]"));*/
		WebElement to=driver.findElement(By.className("thankyou-question"));
		dragAndDrop(driver, from, to);
		System.out.println("Thank You question created");
		addQuestionText(driver,"Thank you");
		
	}
	static void addQuestionText(WebDriver driver,String text) throws InterruptedException {
		Thread.sleep(8000);
		System.out.println("question text insertion start");
		
		driver.switchTo().frame("question-verbiage_ifr");
		WebElement element = driver.findElement(By.id("tinymce"));
		System.out.println("Entering something in text input");
		element.sendKeys(Keys.CONTROL + "a");
		element.sendKeys(text);
	    driver.switchTo().defaultContent();
		
		//driver.findElement(By.xpath("/html/body")).sendKeys("Welcome");
		driver.findElement(By.xpath("/html/body/div[7]/div[14]/div/div/div/div[2]/div/div[2]/div/button[1]")).click();
		
	}
	
	static void addWelcomeText(WebDriver driver) throws InterruptedException {
		Thread.sleep(8000);
		System.out.println("addWelcomeText");
		
		driver.switchTo().frame("question-verbiage_ifr");
		WebElement element = driver.findElement(By.id("tinymce"));
		System.out.println("Entering something in text input");
		element.sendKeys(Keys.CONTROL + "a");
		element.sendKeys("Test text");
	    driver.switchTo().defaultContent();
		
		//driver.findElement(By.xpath("/html/body")).sendKeys("Welcome");
		driver.findElement(By.xpath("/html/body/div[7]/div[14]/div/div/div/div[2]/div/div[2]/div/button[1]")).click();
		
	}
	static void searchSurvey(WebDriver driver) throws InterruptedException
	{
		System.out.println("Inside the search feature");
		driver.findElement(By.xpath("/html/body/div[8]/div[1]/div[8]/div/div/div[1]/div[1]/div/input")).sendKeys("a8787cc1x9");
		Thread.sleep(3000);
		driver.findElement(By.className("fa-edit")).click();
		Thread.sleep(4000);
	}
}
